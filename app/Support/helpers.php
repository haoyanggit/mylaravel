<?php

use Illuminate\Support\Facades\Hash;

if( !function_exists("create_password") ){
    function create_password($value){
        return Hash::make($value);
    }
}

if( !function_exists("check_password") ){
    function check_password($value1,$value2){
        return  Hash::check($value1,$value2);
    }
}