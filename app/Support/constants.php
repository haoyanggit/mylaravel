<?php
/* --------------------------------------------------------------------------------------
 * Definition of Session Keys of Alter box
 * --------------------------------------------------------------------------------------
 *
 * Define the constants of keys which are saved in the current session.
 *
 * The constants can be used anywhere that saving or getting values from session.
 */
const KEY_ALTER_SUCCESS = 'success';
const KEY_ALTER_INFO = 'info';
const KEY_ALTER_WARNING = 'warning';
const KEY_ALTER_ERROR = 'error';

const DEFAULT_PAGE_SIZE = 10;
const DISK_PUBLIC = 'public';

const AREA_CODE_LENGTH = 3;





