<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use QFrame\Controllers\ApiController;
use App\Models\CfgDeliveryNetworks;

/**
 * <apidoc>
 * == 其他接口定义
 * </apidoc>
 * @package App\Http\Controllers\Api
 */
class MiscController 
{
    
    /**
     * <apidoc>
     * === 根据地区code查询县域网点
     *
     * 该接口由客户端APP调用
     *
     * [source, subs=attributes]
     * --
     * [GET] {apiBase}/misc/networks/{code}
     * --
     *
     * .Required Parameters
     * [cols='h,1,3']
     * |===
     * | Name       | Type      | Description
     *
     * | code         | String    | 地区code
     * 
     * |===
     *
     * .Success Response
     * [cols='h,1,3']
     * |===
     * | Name       | Type      | Description
     *
     * | id  | Integer   | 县域网点ID
     * | name  | String   | 县域网点名称
     * 
     * |===
     * </apidoc>
     * @param $sn
     */
    public function networks() {  
    
        return '1';
    }


}
