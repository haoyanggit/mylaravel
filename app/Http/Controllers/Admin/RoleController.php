<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\SysUser;
use App\Models\SysRole;
use App\Models\LogOperation;
use App\Enums\OperationCategory;
use App\Enums\OperationType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\View\Components\NavMenu;


class RoleController extends AdminController
{
  
    public function list() 
    {
        /** @var SysUser $user */
        $roles=SysRole::query()->paginate(DEFAULT_PAGE_SIZE);
        return view('admin.role.list',['roles'=>$roles]);
    }

    public function create(NavMenu $navMenu) 
    {
        $menu=$navMenu->childrens();
        return view('admin.role.create',['menus'=>$menu]);
    }

    public function store(Request $request) 
    {
        Log::debug("Create a new role: ", $request->post());
        $request->validate([
            'name' => 'required|string|unique:sys_roles|max:32',
        ]);

        $role = new SysRole();
        $role->fill([
            'name' => \request('name'),
            'disabled' => \request('disabled'),
            'privileges' => implode(',', \request('privileges')),
        ]);
        $role->save();
        LogOperation::log(Auth::user(), OperationCategory::SystemRole(), OperationType::Create(), true, json_encode($role));
        return redirect()->route('admin.role')
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.save.success', ['name' => trans('entities/role.name')]));

    }


    public function edit($id,NavMenu $navMenu) 
    {
        $menu=$navMenu->childrens();
        $roles=SysRole::query()->whereKey($id)->first();
        return view('admin.role.edit',['roles'=>$roles,'menus'=>$menu]);
    }

    public function update() 
    {
        $role = SysRole::query()->find(\request('id'));
        Log::debug("Update the role: ", $role->jsonSerialize());
        \request()->validate([
            'name' => 'required|string|max:32|unique:sys_roles,name,'.$role->id
        ]);
        $role->fill([
            'name' => \request('name'),
            'disabled' => \request('disabled'),
            'privileges' => implode(',', \request('privileges')),
        ]);
        $role->save();
        LogOperation::log(Auth::user(), OperationCategory::SystemRole(), OperationType::Update(), true, json_encode($role));
        return redirect()->route('admin.role.edit', $role->id)
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.update.success', ['name' => trans('entities/role.name')]));
    }
    
    public function delete($id) 
    {
        $role = SysRole::query()->find($id);
        Log::debug("Delete the role: ", $role->jsonSerialize());
        $role->delete();
        LogOperation::log(Auth::user(), OperationCategory::SystemRole(), OperationType::Delete(), true, json_encode($role));
        return redirect()->route('admin.user')
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.delete.success', ['name' => trans('entities/user.name')]));
    }
    
}
