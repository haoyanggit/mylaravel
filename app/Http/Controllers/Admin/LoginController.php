<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysUser;
use App\Models\LogOperation;
use App\Enums\OperationCategory;
use App\Enums\OperationType;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * 登录
     *
     */
    public function login() 
    {
        return view('admin.login.login');
    }
    
    /**
     *  登录校验
     *
     * @return void
     */
    public function loginCheck(Request $request) 
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
            'captcha' => 'required|captcha',
        ]);
        
        /** @var SysUser $user */
        $user = SysUser::query()->where('username', $request->input('username'))->first();
        if (empty($user) || $user->disabled || !check_password($request->input('password'),$user->password)) {
            if (empty($user)) {
                $user = new SysUser();
                $user->fill([
                    'username' => $request->input('username')
                ]);
            }
            return redirect()->route('admin.login')->with(KEY_ALTER_ERROR, trans('auth.failed'));
        }
        $login_code=$user->createLoginCcode($user->username);
        $user->update(['login_time' => now(), 'login_ip' => $request->getClientIp(), 'login_code'=>$login_code]);
        
        Auth::login($user, $request->input('remember', false));
        LogOperation::log($user, OperationCategory::SystemUser(), OperationType::Login(), true, json_encode(['username'=>$user->username, 'status'=>'登录成功']));

        return redirect()->route('admin.home')
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.system.title', ['message' => trans('app.login_success')]));
        
    }

    /**
     *  退出登录
     *
     * @return void
     */
    public function logout() 
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
