<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\LogOperation;
use App\Enums\OperationCategory;
use App\Enums\OperationType;

class LogsController extends AdminController
{
  
    public function list() 
    {
        $logs=LogOperation::query()->orderBy('id', 'desc')->paginate(DEFAULT_PAGE_SIZE);
        return view('admin.logs.list',['logs'=>$logs]);
    }

    public function show($id) 
    {
        $logs=LogOperation::query()->whereKey($id)->first();
        return view('admin.logs.show',['logs'=> $logs]);
    }

    
}
