<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\View\Composers\NavMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
   
    public function __construct(Request $request) {
        /** 登录状态 */
        $this->middleware('check.login');
    }

}
