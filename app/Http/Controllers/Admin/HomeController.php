<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;

class HomeController extends AdminController
{
    /**
     * 控制台
     *
     */
    public function home() 
    {
        return view('admin.home.home');
    }
    

}
