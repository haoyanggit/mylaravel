<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Http\Request;
use App\Models\SysUser;
use App\Models\SysRole;
use App\Models\LogOperation;
use App\Enums\OperationCategory;
use App\Enums\OperationType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class UserController extends AdminController
{
  
    public function list(Request $request) 
    {
        /** @var SysUser $user */
        $users=SysUser::query()->paginate(DEFAULT_PAGE_SIZE);
        return view('admin.user.list',['users'=>$users]);
    }

    public function create() 
    {
        $roles=SysRole::query()->where('disabled',0)->get();
        return view('admin.user.create',['roles'=>$roles]);
    }

    public function store(Request $request,SysUser $user) 
    {
        Log::debug("Create a new user: ", $request->post());
        $request->validate([
            'username' => 'required|string|unique:sys_users|max:32',
            'password' => 'required|string|max:32|min:6',
            'realname' => 'required|string|max:32'
        ]);
        $user = new SysUser();
        $user->fill([
            'username' => \request('username'),
            'password' => \request('password'),
            'realname' => \request('realname'),
            'role_id' => \request('role'),
            'disabled' => \request('disabled'),
            'password' => create_password(\request('password')),
        ]);
        $user->save();
        LogOperation::log(Auth::user(), OperationCategory::SystemUser(), OperationType::Create(), true, json_encode($user));
        return redirect()->route('admin.user')
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.save.success', ['name' => trans('entities/user.name')]));

    }

    public function edit($id) 
    {
        $user=SysUser::query()->whereKey($id)->first();
        $roles=SysRole::query()->where('disabled',0)->get();
        return view('admin.user.edit',['roles'=>$roles,'user'=> $user]);
    }

    public function update() 
    {
        $user = SysUser::query()->find(\request('id'));
        Log::debug("Update the user: ", $user->jsonSerialize());
        \request()->validate([
            'username' => 'required|string|max:32|unique:sys_users,username,'.$user->id,
            'realname' => 'required|string|max:32',
            'password' => 'nullable|string|max:32|min:6'
        ]);
        $user->fill([
            'username' => \request('username'),
            'realname' => \request('realname'),
            'role_id' => \request('role'),
            'disabled' => \request('disabled')
        ]);
        if (!empty(\request('password'))) {
            $user->password = create_password(\request('password'));
        }
        $user->save();
        LogOperation::log(Auth::user(), OperationCategory::SystemUser(), OperationType::Update(), true, json_encode($user));
        return redirect()->route('admin.user.edit', $user->id)
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.update.success', ['name' => trans('entities/user.name')]));

    }

    public function delete($id) 
    {
        $user = SysUser::query()->find($id);
        Log::debug("Delete the user: ", $user->jsonSerialize());
        $user->delete();
        LogOperation::log(Auth::user(), OperationCategory::SystemUser(), OperationType::Delete(), true, json_encode($user));
        return redirect()->route('admin.user')
                         ->with(KEY_ALTER_SUCCESS, trans('alerts.delete.success', ['name' => trans('entities/user.name')]));
    }
    
    
}
