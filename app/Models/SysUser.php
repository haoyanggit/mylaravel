<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class SysUser extends Authenticatable
{
    use HasFactory;

    public const CREATED_AT = 'create_time';
    public const UPDATED_AT = null;

    protected $fillable = ['username', 'realname', 'disabled', 'role_id', 'login_code', 'login_ip', 'login_time'];

    protected $casts = [
        'disabled' => 'boolean',
        'login_time' => 'datetime',
    ];

    private $privileges = array();

     /**
     *  生成登录标识
     * 
     * @param string $username
     * @return bool
     */
    public function createLoginCcode($username) {
        return  md5('admin_'.$username.time());
    }


    /**
     * @param string $authCode
     * @return bool
     */
    public function hasPrivilege($authCode) {
        if (empty($this->privileges)) {
            $roles = SysRole::query()->whereKey($this->id)->first();
            $this->privileges = explode(',', $roles->privileges);
        }
        return in_array($authCode, $this->privileges);
    }


}
