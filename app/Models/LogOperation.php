<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\OperationCategory;
use App\Enums\OperationType;

class LogOperation extends Model
{
    use HasFactory;

    public const CREATED_AT = 'create_time';
    public const UPDATED_AT = null;

    protected $fillable = ['username', 'category', 'opt', 'successful', 'content', 'from_ip'];

    protected $casts = [
        'successful' => 'boolean',
        'category' => OperationCategory::class,
        'opt' => OperationType::class,
    ];

    /**
     * @param SysUser|Authenticatable|null $user
     * @param OperationCategory $category
     * @param OperationType $opt
     * @param boolean $successful
     * @param string $content
     */
    public static function log($user, $category, $opt, $successful, $content) {
        $entity = new LogOperation();
        $entity->fill([
            'username' => $user->realname,
            'category' => $category,
            'opt' => $opt,
            'successful' => $successful,
            'content' => $content,
            'from_ip' => request()->getClientIp(),
        ]);
        $entity->save();
    }
}
