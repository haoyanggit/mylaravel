<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SysRole extends Model
{
    use HasFactory;

    public const CREATED_AT = 'create_time';
    public const UPDATED_AT = null;

    protected $fillable = ['name', 'disabled', 'privileges'];

    protected $casts = [
        'disabled' => 'boolean'
    ];

    public function hasPrivilege($code) {
        $privileges = explode(',', $this->privileges);
        return in_array($code, $privileges);
    }
    
}
