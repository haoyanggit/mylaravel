<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\SysUser;
use Illuminate\Support\Str;

class NavMenu extends Component
{

    public $menus = [];

    /**
     * Create a new component instance.
     * 添加新的导航按 系统设置 的模式增加
     *
     * @return void
     */
    public function __construct()
    {
        $this->menus = [
            [
                'menu'=> trans('app.menus.dashboard'),
                'link'=> '/admin/home',
                'active'=> false,
                'show'=> true,
                'icon'=> 'fa-home',
                'authCode'=> 'ADMIN_HOME',
                'option'=> []
             ],
             [
                'menu'=> trans('app.menus.system'),
                'link'=> '#',
                'active'=> false,
                'show'=> false,
                'icon'=> 'fa-wrench',
                'authCode'=> '',
                'option'=> [
                    [
                        'menu'=> trans('app.menus.sys_account'),
                        'link'=> '/admin/user',
                        'active'=> false,
                        'show'=> false,
                        'authCode'=> 'ADMIN_USER',
                        
                    ],
                    [
                        'menu'=> trans('app.menus.sys_role'),
                        'link'=> '/admin/role',
                        'active'=> false,
                        'show'=> false,
                        'authCode'=> 'ADMIN_ROLE',
                    ],
                    [
                        'menu'=> trans('app.menus.sys_log'),
                        'link'=> '/admin/logs',
                        'active'=> false,
                        'show'=> false,
                        'authCode'=> 'ADMIN_LOGS',
                    ]       
                ]
             ],
        ];

        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $this->menus=self::item($this->menus);
        return view('components.nav-menu');
    }

    /**
     * 校验权限、是否选中
     *
     * @param array $array
     * @return array
     * 
     */
    private static function item($array) {
        /** @var SysUser $user */
        $user = Auth::user();
        foreach($array as $k=>$v){
            foreach($v['option'] as $kk=>$vv){
                $a_s= self::checkActive($vv['link']);
                if($a_s)  $array[$k]['active']=$a_s; 
                $array[$k]['option'][$kk]['active']=$a_s;

                $s_s= $user->hasPrivilege($vv['authCode']);
                if($s_s)  $array[$k]['show']=$s_s;
                $array[$k]['option'][$kk]['show']=$s_s; 
            }
        }
        return $array;
    }

    public function childrens() {
          $childrens=[];
          foreach($this->menus as $k=>$v){
            foreach($v['option'] as $kk=>$vv){
                array_push($childrens,$vv);
            }
          }
          return $childrens;
    }


    /**
     * 根据当前url判断是否选中
     *
     * @param string $route
     * @return bool
     * 
     */
    private static function checkActive($route) {
        if (empty($route)) {
            return false;
        } 
        $url = request()->path();
        return  Str::startsWith("/$url", $route);
    }

   
}
