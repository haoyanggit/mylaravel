<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static Create()
 * @method static static Update()
 * @method static static Delete()
 * @method static static Disable()
 * @method static static Enable()
 * @method static static Login()
 * @method static static Logout()
 * @method static static Accept()
 * @method static static Refuse()
 */
final class OperationType extends Enum implements LocalizedEnum
{
    const Create =   1;
    const Update = 2;
    const Delete = 3;
    const Disable = 4;
    const Enable = 5;
    const Login = 6;
    const Logout = 7;
    const Accept = 8;
    const Refuse = 9;
}
