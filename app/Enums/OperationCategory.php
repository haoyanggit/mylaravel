<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static SystemUser()
 * @method static static SystemRole()
 */
final class OperationCategory extends Enum implements LocalizedEnum
{
   
    const SystemUser = 1;
    const SystemRole = 2;
   
}
