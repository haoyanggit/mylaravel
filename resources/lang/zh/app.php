<?php

return [
    'home' => '主页',
    'dashboard' => '控制台',
    'system_name' => '后台管理系统',

    'create_time' => '创建时间',
    'update_time' => '更新时间',
    'operation' => '操作',
    'create' => '添加',
    'edit' => '修改',
    'delete' => '删除',
    'see' => '查看',
    'login' => '登录',
    'login_invalid' => '登录失效',
    'login_success' => '登录成功',



    'menus' => [
        'dashboard' => '主页',
        'system' => '系统管理',
        'sys_account' => '管理员管理',
        'sys_role' => '角色管理',
        'sys_log' => '日志查询',
    ],

    'helpers' => [
        'username' => '请输入登录账户',
        'password' => '请输入登录密码',
        'captcha' => '请输入验证码',
    ],
    
];
