<?php

return [
    
    'name' => '角色管理',

    'table' => [
        'name' => '角色',
        'status' => '状态',
    ],

    'labels' => [
        'name' => '角色',
        'privileges' => '权限',
        'status' => '状态',
    ],

    'status' => [
        'enabled' => '启用',
        'disabled' => '禁止',
    ],

 
];

 
