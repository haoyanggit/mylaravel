<?php

return [
    
    'name' => '日志管理',

    'table' => [
        'username' => '用户名',
        'category' => '类别',
        'opt' => '操作',
        'status' => '状态',
    ],

    'labels' => [
        'username' => '登录名',
        'category' => '类别',
        'opt' => '操作',
        'status' => '状态',
        'ip' => 'IP',
    ],

    'status' => [
        'success' => '成功',
        'fail' => '失败',
    ],

 
];

 
