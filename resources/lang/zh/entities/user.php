<?php

return [
    
    'name' => '管理员管理',

    'table' => [
        'username' => '账号',
        'realname' => '用户名',
        'status' => '状态',
    ],

    'labels' => [
        'username' => '登录名',
        'password' => '密码',
        'realname' => '姓名',
        'role' => '角色',
        'status' => '状态',
    ],

    'status' => [
        'enabled' => '启用',
        'disabled' => '禁止',
    ],

 
];

 
