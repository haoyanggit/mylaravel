<?php

use App\Enums\OperationCategory;
use App\Enums\OperationType;

return [
  
    OperationCategory::class => [
        OperationCategory::SystemUser => '管理员管理',
        OperationCategory::SystemRole => '角色管理',
    ],
    OperationType::class => [
        OperationType::Create => '新增',
        OperationType::Update => '编辑',
        OperationType::Delete => '删除',
        OperationType::Disable => '禁用',
        OperationType::Enable => '启用',
        OperationType::Login => '登录',
        OperationType::Logout => '注销',
        OperationType::Accept => '接受',
        OperationType::Refuse => '拒绝',
    ],
];
