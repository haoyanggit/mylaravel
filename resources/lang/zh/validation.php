<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => '必须接受',
    'active_url' => '无效的URL地址',
    'after' => '必须是 :date 之后的日期',
    'after_or_equal' => '必须是 :date 之后或相同的一个日期',
    'alpha' => '只能包含字母',
    'alpha_dash' => '只能包含字母、数字、中划线或下划线',
    'alpha_num' => '只能包含字母和数字',
    'array' => '必须是一个数组',
    'before' => '必须是 :date 之前的一个日期',
    'before_or_equal' => '必须是 :date 之前或相同的一个日期',
    'between' => [
        'numeric' => '必须在 :min 到 :max 之间',
        'file' => '必须在 :min 到 :max KB 之间',
        'string' => '必须在 :min 到 :max 个字符之间',
        'array' => '必须在 :min 到 :max 项之间',
    ],
    'boolean' => '字符必须是 true 或false, 1 或 0',
    'confirmed' => '二次确认不匹配',
    'date' => '必须是一个合法的日期',
    'date_equals' => '必须是与 :date 相同的日期',
    'date_format' => '与给定的格式 :format 不符合',
    'different' => '必须不同于 :other',
    'digits' => '必须是 :digits 位',
    'digits_between' => '必须在 :min 和 :max 位之间',
    'dimensions' => '具有无效的图片尺寸',
    'distinct' => '字段具有重复值',
    'email' => '必须是一个合法的电子邮件地址',
    'ends_with' => '必须以 :values 结束',
    'exists' => '选定的 是无效的.',
    'file' => '必须是一个文件',
    'filled' => '字段是必填的',
    'gt' => [
        'numeric' => '必须大于 :value',
        'file' => '文件必须大于 :value KB.',
        'string' => '必须大于 :value 个字符',
        'array' => '必须大于 :value 项',
    ],
    'gte' => [
        'numeric' => '必须大于等于 :value',
        'file' => '必须大于等于 :value KB.',
        'string' => '必须大于等于 :value 个字符',
        'array' => '必须大于等于 :value 项',
    ],
    'image' => '必须是图片文件',
    'in' => '选定的 是无效的',
    'in_array' => '字段不存在于 :other',
    'integer' => '必须是个整数',
    'ip' => '必须是一个合法的 IP 地址。',
    'ipv4' => '必须是一个合法的 IPv4 地址',
    'ipv6' => '必须是一个合法的 IPv6 地址',
    'json' => '必须是一个合法的 JSON 字符串',
    'lt' => [
        'numeric' => '必须小于 :value.',
        'file' => '必须小于 :value KB.',
        'string' => '必须小于 :value 个字符',
        'array' => '必须小于 :value 项',
    ],
    'lte' => [
        'numeric' => '必须小于等于 :value.',
        'file' => '必须小于等于 :value KB',
        'string' => '必须小于等于 :value 个字符',
        'array' => '必须小于等于 :value 项',
    ],
    'max' => [
        'numeric' => '最大长度为 :max 位',
        'file' => '最大为 :max KB',
        'string' => '最大长度为 :max 字符',
        'array' => '最大个数为 :max 个',
    ],
    'mimes' => '文件类型必须是 :values',
    'mimetypes' => '文件类型必须是 :values',
    'min' => [
        'numeric' => '最小长度为 :min 位',
        'file' => '大小至少为 :min KB',
        'string' => '最小长度为 :min 字符',
        'array' => '至少有 :min 项',
    ],
    'not_in' => '选定的 是无效的',
    'not_regex' => '格式无效',
    'numeric' => '必须是数字',
    'password' => '密码错误',
    'present' => '字段必须存在',
    'regex' => '格式无效',
    'required' => '字段是必须的',
    'required_if' => '字段是必须的当 :other 是 :value',
    'required_unless' => '字段是必须的，除非 :other 是在 :values 中',
    'required_with' => '字段是必须的当 :values 是存在的',
    'required_with_all' => '字段是必须的当 :values 是存在的',
    'required_without' => '字段是必须的当 :values 是不存在的',
    'required_without_all' => '字段是必须的当 没有一个 :values 是存在的',
    'same' => '和:other必须匹配',
    'size' => [
        'numeric' => '必须是 :size 位',
        'file' => '必须是 :size KB',
        'string' => '必须是 :size 个字符',
        'array' => '必须包括 :size 项',
    ],
    'starts_with' => '必须以 :values 做为前缀',
    'string' => '必须是一个字符串',
    'timezone' => '必须是个有效的时区',
    'unique' => '已存在',
    'uploaded' => '上传失败',
    'url' => '无效的格式',
    'uuid' => '必须是有效的 UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'captcha' => '验证码错误',
    'strong_password' => '密码必须包含大小写字母及数字，且不包含特殊字符',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'password' => '登录密码',
    ],

];
