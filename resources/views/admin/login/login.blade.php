<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ __('app.login') }}</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/static/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/static/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/static/plugins/admin-lte/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/static/plugins/toastr/toastr.min.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="javascript:;" class="h1">{{ __('app.login') }}</a>
    </div>
    <div class="card-body">
      <form action="{{ route('admin.loginCheck') }}" method="post">
        @csrf
        @if(session('error'))
          <div class="alert alert-danger">{{ session('error') }}</div>
         @endif
        <div class="input-group mb-3">          
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          <input type="text"  class="form-control" name="username" placeholder="{{ __('app.helpers.username') }}" required value="admin">
          @error('username')
          <span class="error invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input type="password" class="form-control" name="password" placeholder="{{ __('app.helpers.password') }}" required value="admin">
          @error('password')
          <span class="error invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="captcha" placeholder="{{ __('app.helpers.captcha') }}" required >
          <img src="{{captcha_src()}}" style="cursor: pointer" onclick="this.src='{{captcha_src()}}'+Math.random()">
          @error('captcha')
          <span class="error invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></span>
          @enderror
        </div>
        
        

        <div class="row">
          <button type="submit" class="btn btn-primary btn-block">{{ __('app.login') }}</button>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('/static/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/static/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/static/plugins/admin-lte/js/adminlte.min.js') }}"></script>
<script src="{{ asset('/static/plugins/toastr/toastr.min.js') }}"></script>
@if(session('warning'))
    <script>toastr.warning('{{ session('warning') }}');</script>
@endif
</body>
</html>
