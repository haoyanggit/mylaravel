@extends('admin.layouts.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('app.menus.sys_account') }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ __('app.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('app.menus.sys_account') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="flex: 100%;max-width: 100%;">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-header">
                <h3 class="card-title">
                 <a href="{{ route('admin.user.create') }}" class="btn btn-block btn-primary">{{ __('app.create') }}</a>
                </h3>
              </div>
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">ID</th>
                      <th>{{ __('entities/user.table.username') }}</th>
                      <th>{{ __('entities/user.table.realname') }}</th>
                      <th>{{ __('entities/user.table.status') }}</th>
                      <th>{{ __('app.create_time') }}</th>
                      <th>{{ __('app.operation') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $user)
                    <tr>
                      <td>{{ $user->id }}</td>
                      <td>{{ $user->username }}</td>
                      <td>{{ $user->realname }}</td>
                      <td>
                        @if($user->disabled)
                            <span class="badge bg-gray">{{ __('entities/user.status.disabled') }}</span>
                        @else
                            <span class="badge bg-green">{{ __('entities/user.status.enabled') }}</span>
                        @endif
                      </td>
                      <td>{{ $user->create_time }}</td>
                      <td>
                        <a class="btn btn-primary btn-sm" href="{{ route('admin.user.edit', $user->id) }}" title="{{ __('app.edit') }}">
                            <i class="fas fa-pencil-alt"></i>                          
                        </a>
                        @if ($user->id != 1)
                        <form action="{{ route('admin.user.delete', $user->id) }}" method="POST" style="display: inline;">
                          @csrf
                          <a class="btn btn-danger btn-sm x-delete" href="#" title="{{ __('app.delete') }}" data-id="{{ $user->id }}"><i class="fas fa-trash"></i></a>
                        </form>                        
                        @endif
                    </td>
                    </tr>  
                    @endforeach             
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                {{ $users->links() }}
              </div>
            </div>
          </div>
        </div> 
      </div>
    </section>
  </div>
  @endsection

  @push('scripts')
    <script type="application/javascript">
        $(function () {
            $('.x-delete').on('click', function (event) {
                console.info('Click the button to delete a Advert Picture.');
                event.preventDefault();
                const $form = $(this).parent('form');

                Swal.fire({
                    title: '{{ __('confirm.delete', ['name'=>'']) }}',
                    text: '{{ __('confirm.delete.description') }}',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{ __('confirm') }}',
                    cancelButtonText: '{{ __('cancel') }}'
                }).then(function(result) {
                    if(result.value) {
                        $form.submit();
                    }
                });
            });
        })
    </script>
  @endpush