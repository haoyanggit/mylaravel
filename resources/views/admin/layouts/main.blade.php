@extends('admin.layouts.base')

@section('body')
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link"  href="#" role="button">
           {{ Auth::user()->realname }}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link btn btn-block btn-secondary"  href="{{ route('admin.logout') }}" style="color:#fff">
           退出          
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.home') }}" class="brand-link">
      <span class="brand-text font-weight-light">{{ __('app.system_name') }}</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <x-nav-menu/>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  
  <!-- /.content-wrapper -->

</div>
@endsection

@push('scripts')
    @if(session('success'))
        <script>toastr.success('{{ session('success') }}');</script>
    @endif
    @if(session('info'))
        <script>toastr.info('{{ session('info') }}');</script>
    @endif
    @if(session('error'))
        <script>toastr.error('{{ session('error') }}');</script>
    @endif
    @if(session('warning'))
        <script>toastr.warning('{{ session('warning') }}');</script>
    @endif

    <script>
        $(function () {
            $('.x-logout').on('click', function (event) {
                event.preventDefault();
                $('#frm-logout').submit();
            });
        });
    </script>
@endpush