<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>后台管理系统</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href=" {{ asset('/static/plugins/fontawesome-free/css/all.min.css') }} ">

  <!-- Ionicons -->
  <link rel="stylesheet" href=" {{ asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }} ">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href=" {{ asset('/static/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }} ">
  <!-- iCheck -->
  <link rel="stylesheet" href=" {{ asset('/static/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }} ">
  <!-- Theme style -->
  <link rel="stylesheet" href=" {{ asset('/static/plugins/admin-lte/css/adminlte.min.css') }} ">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href=" {{ asset('/static/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }} ">
  <!-- Daterange picker -->
  <link rel="stylesheet" href=" {{ asset('/static/plugins/daterangepicker/daterangepicker.css') }} ">
  <link rel="stylesheet" href="{{ asset('/static/plugins/toastr/toastr.min.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  @yield('body')

<!-- ./wrapper -->

<!-- jQuery -->
<script src=" {{ asset('/static/plugins/jquery/jquery.min.js') }} "></script>
<!-- jQuery UI 1.11.4 -->
<script src=" {{ asset('/static/plugins/jquery-ui/jquery-ui.min.js') }} "></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src=" {{ asset('/static/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- ChartJS -->
<script src=" {{ asset('/static/plugins/chart.js/Chart.min.js') }} "></script>
<!-- Sparkline -->
<script src=" {{ asset('/static/plugins/sparklines/sparkline.js') }} "></script>
<!-- daterangepicker -->
<script src=" {{ asset('/static/plugins/moment/moment.min.js') }} "></script>
<script src=" {{ asset('/static/plugins/daterangepicker/daterangepicker.js') }} "></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src=" {{ asset('/static/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }} "></script>
<!-- Summernote -->
<script src=" {{ asset('/static/plugins/summernote/summernote-bs4.min.js') }} "></script>
<!-- overlayScrollbars -->
<script src=" {{ asset('/static/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }} "></script>
<!-- bs-custom-file-input -->
<script src="{{ asset('/static/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<script src="{{ asset('/static/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('/static/plugins/toastr/toastr.min.js') }}"></script>
<!-- AdminLTE App -->
<script src=" {{ asset('/static/plugins/admin-lte//js/adminlte.js') }} "></script>
@stack('scripts')
</body>
</html>
