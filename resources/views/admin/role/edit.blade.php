@extends('admin.layouts.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('app.menus.sys_role') }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ __('app.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('app.menus.sys_role') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">{{ __('app.edit') }}</h3>
            </div>
            <form action="{{ route('admin.role.update') }}" method="post">
              @csrf
            <input type="hidden" id="id" name="id" class="form-control" required  value="{{ $roles->id }}">
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">{{ __('entities/role.labels.name') }}</label>
                <input type="text" id="name" name="name" class="form-control" required  value="{{ $roles->name }}">
                @error('name')
                <span class="error invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
              </div>
              <div class="form-group">
                <label for="inputName">{{ __('entities/role.labels.privileges') }}</label>
                @foreach ($menus as $menu)
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" name="privileges[]" id="chk_{{ $menu['authCode'] }}" value="{{ $menu['authCode'] }}" @if($roles->hasPrivilege($menu['authCode'])) checked @endif>
                  <label for="chk_{{ $menu['authCode'] }}" class="custom-control-label">{{ $menu['menu'] }}</label>
                </div>
                @endforeach         
              </div>
              <div class="form-group">
                <label for="inputName">{{ __('entities/role.labels.status') }}</label>
                <div class="custom-control custom-radio">
                  <input class="custom-control-input" type="radio" id="customRadio1" name="disabled" value="0"  @if($roles->disabled === false) checked="" @endif >
                  <label for="customRadio1" class="custom-control-label">{{ __('entities/role.status.enabled') }}</label>
                </div>
                <div class="custom-control custom-radio">
                  <input class="custom-control-input" type="radio" id="customRadio2" name="disabled"  value="1" @if($roles->disabled === true) checked="" @endif >
                  <label for="customRadio2" class="custom-control-label">{{ __('entities/role.status.disabled') }}</label>
                </div>
                @error('disabled')
                <span class="error invalid-feedback d-block" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
              </div>
            </div>    
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">{{ __('app.edit') }}</button>
            </div>
          </form>
        </div>          
          <!-- /.card -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection