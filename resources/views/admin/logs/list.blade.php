@extends('admin.layouts.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('entities/logs.name') }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ __('app.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('entities/logs.name') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6" style="flex: 100%;max-width: 100%;">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">ID</th>
                      <th>{{ __('entities/logs.table.username') }}</th>
                      <th>{{ __('entities/logs.table.category') }}</th>
                      <th>{{ __('entities/logs.table.opt') }}</th>
                      <th>{{ __('entities/logs.table.status') }}</th>
                      <th>{{ __('app.create_time') }}</th>
                      <th>{{ __('app.operation') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($logs as $log)
                    <tr>
                      <td>{{ $log->id }}</td>
                      <td>{{ $log->username }}</td>  
                      <td>{{ $log->category->description }}</td>  
                      <td>{{ $log->opt->description }}</td>  
                      <td>
                        @if($log->successful)
                            {{ __('entities/logs.status.success') }}
                        @else
                            {{ __('entities/logs.status.fail') }}
                        @endif                      
                      </td>  
                      <td>{{ $log->create_time }}</td>  
                      <td>
                        <a class="btn btn-primary btn-sm" href="{{ route('admin.logs.show', $log->id) }}" title="{{ __('app.see') }}">
                            <i class="fas fa-eye"></i>                          
                        </a> 
                    </td>
                    </tr>  
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                {{ $logs->links() }}
              </div>
            </div>
          </div>
        </div> 
      </div>
    </section>
  </div>
  @endsection