@extends('admin.layouts.main')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ __('entities/logs.name') }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">{{ __('app.home') }}</a></li>
              <li class="breadcrumb-item active">{{ __('entities/logs.name') }}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-info">
            <div class="card-body p-0">
              <table class="table">
                <tbody>
                  <tr>
                    <td>{{ __('entities/logs.labels.username') }}</td>
                    <td>{{ $logs->username }}</td>
                  <tr>
                    <td>{{ __('entities/logs.labels.category') }}</td>
                    <td>{{ $logs->category->description }}</td>            
                  <tr>
                    <td>{{ __('entities/logs.labels.opt') }}</td>
                    <td>{{ $logs->opt->description }}</td>
                  <tr>
                    <td>{{ __('entities/logs.labels.status') }}</td>
                    <td>
                      @if($logs->successful)
                          {{ __('entities/logs.status.success') }}
                      @else
                          {{ __('entities/logs.status.fail') }}
                      @endif   
                    </td>
                  <tr>
                    <td>{{ __('entities/logs.labels.ip') }}</td>
                    <td>{{ $logs->from_ip }}</td>
                    <tr>
                      <td>{{ __('app.create_time') }}</td>
                      <td>{{ $logs->create_time }}</td>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-info">
            <div class="card-body p-0">
              <table class="table">
                <tbody>
                  @foreach(json_decode($logs->content, true) as $key=>$value)
                  <tr>
                    <td>{{ $key }}</td>
                    <td>{{ $value }}</td>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection