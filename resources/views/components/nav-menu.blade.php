<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      @foreach ($menus as $menu)
      @if($menu['show'])
      <li class="nav-item  @if($menu['active']) menu-open  @endif">
        <a href="{{ $menu['link'] }}" class="nav-link @if($menu['active']) active @endif">
          <i class="nav-icon fas {{ $menu['icon'] }}"></i>
          <p>
            {{ $menu['menu'] }}
          </p>
        </a>
        @if(!empty($menu['option']))
        <ul class="nav nav-treeview">
          @foreach ($menu['option'] as $option)
          @if($option['show'])
          <li class="nav-item">
            <a href="{{ $option['link'] }}" class="nav-link @if($option['active']) active @endif">
              <i class="far fa-circle nav-icon"></i>
              <p>{{ $option['menu'] }}</p>
            </a>
          </li>
          @endif
          @endforeach
        </ul>
        @endif
      </li>
      @endif
      @endforeach
    </ul>
  </nav>