<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateBaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_roles', function (Blueprint $table) {
            $table->id();
            $table->string('name', 64)->unique();
            $table->text('privileges');
            $table->tinyInteger('disabled')->default(0);
            $table->timestamp('update_time')->useCurrent();
            $table->timestamp('create_time')->useCurrent();

        });

        Schema::create('sys_users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 32)->unique();
            $table->string('realname', 64);
            $table->string('password', 64);
            $table->tinyInteger('disabled')->default(0);
            $table->foreignId('role_id');
            $table->string('login_code', 64)->nullable();
            $table->string('login_ip', 32)->nullable();
            $table->timestamp('login_time')->nullable();
            $table->timestamp('create_time')->useCurrent();

            $table->foreign('role_id', 'FK_USER_ROLE_RID')->references('id')->on('sys_roles');
        });

        DB::table('sys_roles')->insert([
            'id'=>1,
            'name' => '超级管理员',
            'privileges' => 'ADMIN_HOME,ADMIN_USER,ADMIN_ROLE,ADMIN_lOGS',
            'disabled' => false,
            'create_time' => now(),
        ]);

        DB::table('sys_users')->insert([
            'id'=>1,
            'username' => 'admin',
            'realname' => '超级管理员',
            'password' => create_password('admin'),
            'role_id' => 1,
            'disabled' => false,
            'create_time' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_roles');
        Schema::dropIfExists('sys_users');
    }
}
