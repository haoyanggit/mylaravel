<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationLoggingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_operations', function (Blueprint $table) {
            $table->id();
            $table->string('username', 32);
            $table->tinyInteger('category');
            $table->tinyInteger('opt');
            $table->string('from_ip', 32);
            $table->text('content');
            $table->tinyInteger('successful')->default(1);
            $table->timestamp('create_time')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_logging');
    }
}
