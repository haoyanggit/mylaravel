## 部署说明

> 运行环境要求PHP7.3+ 。

```lang=bash
git clone 项目地址
```

执行如下操作：

```lang=bash
cp .env.example .env
```

修改 `.env` 文件：
- 修改 `APP_KEY` 添加一个秘钥（大小写字母、数字组合；长度建议不超过60）
- 修改 `APP_URL` 为实际通过浏览器可访问的地址，如：`http://www.域名.com`
- 修改 `DB_HOST` 为数据库的IP地址
- 修改 `DB_DATABASE` 为前面建立的新的库名
- 修改 `DB_USERNAME` 和 `DB_PASSWORD` 为实际可访问数据库的帐号、密码

~~~
执行 composer install
~~~

~~~
运行数据库迁移文件 php artisan migrate
~~~

nginx配置：
- 目录指向 `public` 文件
- conf 配置文件 配置伪静态，隐藏 index.php


## 后台登录默认

登录入口: 域名/admin/login

默认账号：`admin`  密码：`admin`