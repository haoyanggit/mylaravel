<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\LogsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** 后台 */
Route::name('admin.')->group(function () {
    /** 登录 */
    Route::get('admin/login', [LoginController::class, 'login'])->name('login');
    Route::post('admin/loginCheck', [LoginController::class, 'loginCheck'])->name('loginCheck');
    Route::get('admin/logout', [LoginController::class, 'logout'])->name('logout');

    /** 控制台 */
    Route::get('admin/home', [HomeController::class, 'home'])->name('home');
    /** 管理员 */
    Route::get('admin/user', [UserController::class, 'list'])->name('user');
    Route::get('admin/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('admin/user/store', [UserController::class, 'store'])->name('user.store');
    Route::get('admin/user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::post('admin/user/update', [UserController::class, 'update'])->name('user.update');
    Route::post('admin/user/delete/{id}', [UserController::class, 'delete'])->name('user.delete');
    /** 角色 */
    Route::get('admin/role', [RoleController::class, 'list'])->name('role');
    Route::get('admin/role/create', [RoleController::class, 'create'])->name('role.create');
    Route::post('admin/role/store', [RoleController::class, 'store'])->name('role.store');
    Route::get('admin/role/edit/{id}', [RoleController::class, 'edit'])->name('role.edit');
    Route::post('admin/role/update', [RoleController::class, 'update'])->name('role.update');
    Route::post('admin/role/delete/{id}', [RoleController::class, 'delete'])->name('role.delete');
    /** 日志 */
    Route::get('admin/logs', [LogsController::class, 'list'])->name('logs');
    Route::get('admin/logs/show/{id}', [LogsController::class, 'show'])->name('logs.show');

});
